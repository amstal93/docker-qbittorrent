FROM alpine:edge

ARG QBT_VER
ARG LIB_VER

ENV PEER_PORT=6881 \
    WEB_PORT=8080 \
    UID=1000 \
    GID=1000

RUN set -e && \
    apk add --no-cache su-exec && \
    apk add --no-cache --virtual .build-deps \
        boost-dev \
        boost-system \
        boost-thread \
        boost-build \
        ca-certificates \
        curl \
        g++ \
        libressl-dev \
        make \
        qt5-qtbase \
        qt5-qttools-dev \
        tar && \
    mkdir -p /tmp/libtorrent-rasterbar && \
    cd /tmp/libtorrent-rasterbar/ && \
    curl -sSL ${LIB_VER} | tar xz --strip 1 && \
    export CORES=$(nproc) && \
    b2 install -j${CORES} --prefix=/usr --libdir=/usr/lib release \
    	toolset=gcc \
    	cxxstd=17 \
    	strip=on \
    	logging=off \
    	dht=on \
    	asserts=off  \
    	encryption=on \
    	crypto=openssl \
    	invariant-checks=off \
    	i2p=off \
    	profile-calls=off \
    	streaming=off \
    	super-seeding=off && \
    mkdir -p /tmp/qbittorrent && \
    cd /tmp/qbittorrent && \
    curl -sSL https://github.com/qbittorrent/qBittorrent/archive/${QBT_VER}.tar.gz | tar xz --strip 1 && \
	libtorrent_LIBS="/usr/lib/libtorrent-rasterbar.so" libtorrent_CFLAGS="-I/usr/include/libtorrent" ./configure \
    	CXXFLAGS="-std=c++17" \
    	--prefix=/usr \
    	--disable-dependency-tracking \
    	--enable-silent-rules \
    	--disable-gui \
    	--disable-qt-dbus \
    	--disable-debug \
    	--disable-stacktrace && \
    make -j${CORES} install && \
    cd / && \
    runDeps="$( \
        scanelf --needed --nobanner /usr/lib/libtorrent* /usr/bin/qbittorrent* \
            | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
            | xargs -r apk info --installed \
            | sort -u \
    )" && \
    apk add --no-cache --virtual .run-deps $runDeps && \
    apk del .build-deps && \
    rm -rf /tmp/*

COPY rootfs /

ENTRYPOINT ["/usr/bin/entrypoint.sh"]
CMD ["/usr/bin/qbittorrent-nox"]
